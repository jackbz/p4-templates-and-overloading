//
//  NumberClass.h
//  CommandLineTool
//
//  Created by Jack on 19/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef NumberClass_h
#define NumberClass_h


template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value ";
        return value;
    }
    void set(Type newValue)
    {
        value = newValue;
    }
private:
    Type value;
};

#endif /* NumberClass_h */
