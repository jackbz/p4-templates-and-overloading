//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "Classes.hpp"
#include "NumberClass.h"



int main()
{
    Array<float> test1, test2;
    
    test1.add(4);
    test1.add(68686);
    test1.add(5);
    test1.add(6);
    test1.add(7);
    
    test2.add(4);
    test2.add(4);
    test2.add(5);
    test2.add(6);
    test2.add(7);
    
    std::cout << test1[2] << " " << test1[1];
}
