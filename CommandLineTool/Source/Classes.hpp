//
//  Classes.hpp
//  CommandLineTool
//
//  Created by Jack on 07/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef Classes_hpp
#define Classes_hpp

#include <stdio.h>
#include <cmath>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////////////////////

template <class Type>
class Array
{
public:
    /**Constructor*/
    Array()
    {
        arraySize = 0;
        currentArray = nullptr;
    }
    
    /**Destructor*/
    ~Array()
    {
        if (currentArray != nullptr)
            delete[] currentArray;
    }
    
    /**Adds new items to the end of the array*/
    void add (Type itemValue)
    {
        //allocates a new array whose size is size + 1
        Type* tempArray = new Type[arraySize+1];
        
        //copies contents of the current array to a temporary one
        for (int i = 0; i < arraySize; i++)
        {
            tempArray[i] = currentArray[i];
        }
        
        //this temp array has a variable size, so copy the new value to the end of this one
        tempArray[arraySize] = itemValue;
        
        //delete all the date in the current array
        if (currentArray != nullptr)
        {
            delete[] currentArray;
        }
        
        //copy the data from the dynamic, temporary array into the actual array we use
        currentArray = tempArray;
        
        //incremement the array size by 1
        arraySize++;
    }
    
    /**Returns the item at the index*/
    Type operator[] (int index) const
    {
        if (index >= 0 && index < arraySize)
            return currentArray[index];
        else
            return 0;
    }
    
    /**Returns the number of items currently in the array*/
    int size() const
    {
        return arraySize;
    }
    
    /**Removes an item from the array*/
    void remove(int index)
    {
        if (index < 0 || index >= arraySize)
            return;
        
        //allocates a new array whose size is size - 1
        Type* tempArray = new Type[arraySize-1];
        
        //copies contents of the current array to a temporary one up until index that needs deleting
        for (int i = 0; i < arraySize; i++)
        {
            if (i < index)
                tempArray[i] = currentArray[i];
            else if (i >index)
                tempArray[i-1] = currentArray[i];
        }
        
        //delete all the date in the current array
        delete[] currentArray;
        
        //copy the data from the dynamic, temporary array into the actual array we use
        currentArray = tempArray;
        
        //decrement the array size by 1
        arraySize--;
    }
    
    /**Reverses order of elements in array*/
    void reverse()
    {
        int halfArraySize = size() / 2; //odd numbers leave the middle element where it is
        for (int i = 0; i < halfArraySize; i++)
        {
            Type temp = currentArray[i];
            currentArray[i] = currentArray[size() - 1 - i];
            currentArray[size() - 1 - i] = temp;
        }
    }
    
//    /** Over loads the == operator*/
//    bool operator==(const Array& otherArray)
//    {
//       
//        if (size() == otherArray.size())
//        {
//            for (int i = 0; i < size(); i++)
//            {
//                if (get(i) != otherArray.get(i))
//                {
//                    return false;
//                }
//            }
//            return true;
//        }
//        return false;
//    }
    
    /** Over loads the == operator*/
    
//    bool operator!=(const Array& otherArray)
//    {
//        
//        if (size() == otherArray.size())
//        {
//            for (int i = 0; i < size(); i++)
//            {
//                if (get(i) != otherArray.get(i))
//                {
//                    return true;
//                }
//            }
//            return false;
//        }
//        return true;
//    }
    
    /**Tests array to see if everything works*/
    static bool testArray()
    {
        Array array;
        const Type testArray[] = {0, 1, 2, 3, 4, 5};
        const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
        
        if (array.size() != 0)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        for (int i = 0; i < testArraySize; i++)
        {
            array.add (testArray[i]);
            
            if (array.size() != i + 1)
            {
                std::cout << "size is incorrect\n";
                return false;
            }
            
            if (array.get (i) != testArray[i])
            {
                std::cout << "value at index "<< i << " recalled incorrectly\n" ;
                return false;
            }
        }
        
        array.reverse();
        for (int i = 0; i < testArraySize; i++)
        {
            if (array.get(i) != testArray[testArraySize - 1 - i])
            {
                std::cout << "reverse did not work\n";
                return false;
            }
        }
        //put back forward
        array.reverse();
        
        //removing first
        array.remove (0);
        if (array.size() != testArraySize - 1)
        {
            std::cout << "with size after removing item\n";
            return false;
        }
        
        for (int i = 0; i < array.size(); i++)
        {
            if (array.get(i) != testArray[i+1])
            {
                std::cout << "problems removing items\n";
                return false;
            }
        }
        
        //removing last
        array.remove (array.size() - 1);
        if (array.size() != testArraySize - 2)
        {
            std::cout << "with size after removing item\n";
            return false;
        }
        
        for (int i = 0; i < array.size(); i++)
        {
            if (array.get(i) != testArray[i + 1])
            {
                std::cout << "problems removing items\n";
                return false;
            }
        }
        
        //remove second item
        array.remove (1);
        if (array.size() != testArraySize - 3)
        {
            std::cout << "with size after removing item\n";
            return false;
        }
        
        if (array.get (0) != testArray[1])
        {
            std::cout << "problems removing items\n";
            return false;
        }
        
        if (array.get (1) != testArray[3])
        {
            std::cout << "problems removing items\n";
            return false;
        }
        
        if (array.get (2) != testArray[4])
        {
            std::cout << "problems removing items\n";
            return false;
        }
        
        //    for (int i = 0; i < array.size(); i++)
        //    {
        //        std::cout << "value at index "<< i << " = " << array.get (i) << "\n";
        //    }
        
        std::cout << "all Array tests passed\n";
        return true;
 
    }
    
private:
    int arraySize;
    Type* currentArray;
};


//////////////////////////////////////////////////////////////////////////////////////////////////

//LinkedList::LinkedList()
//{
//    head = nullptr;
//    tail = nullptr;
//    listSize = 0;
//}
//LinkedList::~LinkedList()
//{
//    delete[] head;
//    delete[] tail;
//}
//void LinkedList::add (float itemValue)
//{
//    Node* tempNode = new Node;
//    tempNode->value = itemValue;
//    tempNode->next = nullptr;
//    
//    if(head == nullptr)
//    {
//        head = tempNode;
//        tail = tempNode;
//    }
//    else
//    {
//        tail->next = tempNode;
//        tail = tail->next;
//    }
//    
//}
//float LinkedList::get (int index) const
//{
//    if (index >= listSize)
//    {
//        std::cout << "\nERROR: NODE DOESN'T EXIST\n";
//        return 0;
//    }
//    else
//    {
//        Node* tempNode;
//        tempNode = head;
//        int count = 0;
//        while (count != index+1)
//        {
//            count++;
//            tempNode = tempNode->next;
//        }
//        return tempNode->value;
//    }
//}
//int LinkedList::size()
//{
//    Node* tempNode;
//    tempNode = head;
//    int index = 0;
//    while (tempNode != nullptr)
//    {
//        index++;
//        tempNode = tempNode->next;
//    }
//    listSize = index;
//    return listSize;
//    
//}
//void LinkedList::display()
//{
//    Node* tempNode;
//    tempNode = head;
//    while (tempNode != nullptr)
//    {
//        std::cout << tempNode->value << std::endl;
//        tempNode = tempNode->next;
//    }
//}
//
//void LinkedList::remove(int index)
//{
//    int count = 0;
//    
//    //cycles through list until before the node that needs deleting
//    while (count != index-1)
//    {
//        count++;
//        tail = tail->next;
//    }
//    
//    //skips over the node that is gonna be deleted
//    tail = tail->next->next;
//    
//    
//    //continues until it gets to nullptr or someething who knows
//    while (tail != nullptr)
//    {
//        tail = tail->next;
//    }
//    
//}
//
//void LinkedList::reverse()
//{
//    
//}

//////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////

class LinkedList
{
public:
    /**Constructor*/
    LinkedList();
    
    /**Destructor*/
    ~LinkedList();
    
    /**Adds new items to the end of the list*/
    void add (float itemValue);
    
    /**Returns the item stored in the node at the specified index*/
    float get (int index) const;
    
    /**Returns the number of items currently in the array*/
    int size();
    
    /**Displays every item in list in order */
    void display();
    
    /**Removes an item from the list*/
    void remove(int index);
    
    /**Reverses item order of list*/
    void reverse();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
    Node* tail;
    int listSize;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif /* Classes_hpp */
